# Generated by Django 4.0.5 on 2022-07-04 11:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0031_alter_direccionentrega_fecha_enviado'),
    ]

    operations = [
        migrations.AddField(
            model_name='direccionentrega',
            name='cantidad',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
