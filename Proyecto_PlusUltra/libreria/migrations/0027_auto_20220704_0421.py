# Generated by Django 3.2.8 on 2022-07-04 04:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0026_auto_20220704_0406'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orden',
            name='producto',
        ),
        migrations.AddField(
            model_name='direccionentrega',
            name='producto',
            field=models.CharField(default=True, max_length=200),
            preserve_default=False,
        ),
    ]
